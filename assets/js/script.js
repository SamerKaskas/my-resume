
function show_menu() {
  $('#show_menuitems').addClass('active');
}

function hide_menu() {
  $('#show_menuitems').removeClass('active');
}
// --------------------------------------

$(document).ready(function() {
  var owl = $('.owl-carousel');
  owl.owlCarousel({
  items: 1,
  loop: true,
  margin: 10,
  autoplay: true,
  autoplayTimeout: 10000,
  autoplayHoverPause: true
  });
});

$(document).ready(function(){
  $("#html").on({
    mouseenter: function(){
        $(this).css("color", "#de4610");
        $(this).animate({ top: "-15" });
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#css").on({
    mouseenter: function(){
        $(this).css("color", "#42a5f5");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#js").on({
    mouseenter: function(){
        $(this).css("color", "#f0db4f");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#php").on({
    mouseenter: function(){
        $(this).css("color", "#8892BF");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#mysql").on({
    mouseenter: function(){
        $(this).css("color", "#3E6E93");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#angular").on({
    mouseenter: function(){
        $(this).css("color", "#a6120d");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#react").on({
    mouseenter: function(){
        $(this).css("color", "#61dafb");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#nodejs").on({
    mouseenter: function(){
        $(this).css("color", "#43853d");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#sass").on({
    mouseenter: function(){
        $(this).css("color", "#bf4080");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#bootstrap").on({
    mouseenter: function(){
        $(this).css("color", "#563d7c");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#docker").on({
    mouseenter: function(){
        $(this).css("color", "#0db7ed");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#git").on({
    mouseenter: function(){
        $(this).css("color", "#f34f29");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
  $("#java").on({
    mouseenter: function(){
        $(this).css("color", "#259797");
    },  
  
    mouseleave: function(){
        $(this).css("color", "#212529");
    }
  });
});
// --------------------------------------
